# stripes-kint-components
A repository to hold components designed to bridge the gap between K-Int's web toolkit based backend modules and stripes.

## Installation and usage
Simply run
```
yarn add @k-int/stripes-kint-components
```
This repository is available as a public NPM and is pre-transpiled, so it should be ready to work immediately.

In the case where components from this repository require translations, it will need to be added to the `stripesDeps` array in the implementing module's `package.json`.

## Development
In order to develop this module within a stripes workspace, a couple of steps are needed
- An environment variable must be set up (See https://chlee.co/how-to-setup-environment-variables-for-windows-mac-and-linux/), `STRIPES_TRANSPILE_TOKENS="@k-int"`
- Clone this repository `git clone git@gitlab.com:knowledge-integration/folio/stripes-kint-components.git`
- Delete all the `devDependencies` in your local package.json. (DO NOT MERGE THESE CHANGES BACK TO MASTER)
- Nuke your workspace node_modules and rebuild

### What went wrong
- If you aren't seeing changes take effect double check
  - Your environment variable is definitely set `printenv STRIPES_TRANSPILE_TOKENS` on Linux, Windows may differ
  - The `es` directory does not exist inside your local copy of kint-components. When built, this directory is constructed, and will take precedence if present, so if present, always delete for development work
- If you aren't seeing translations properly, ensure stripes-kint-components is present in the stripesDeps of your implementing module

## Documentation
The gitlab repository for this project can be found [here](https://gitlab.com/knowledge-integration/folio/stripes-kint-components).

### Quick links
  - [Components](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/-/tree/main/src/lib)
  - [Hooks](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/-/tree/main/src/lib/hooks)
  - [Utility functions](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/-/tree/main/src/lib/utils)
