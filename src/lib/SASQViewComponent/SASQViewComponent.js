import { forwardRef, useImperativeHandle } from 'react';
import PropTypes from 'prop-types';

import { useQuery } from 'react-query';

import {
  useNamespace,
  useOkapiKy
} from '@folio/stripes/core';

const SASQViewComponent = forwardRef(({
  fetchParameters,
  history,
  id,
  location,
  match,
  path,
  ViewComponent,
  ...props
}, ref) => {
  const { 0: namespace } = useNamespace();

  // If itemEndpoint is available, use that, otherwise use standard endpoint
  const endpoint = fetchParameters?.itemEndpoint ?? fetchParameters?.endpoint;

  const queryNamespace = [namespace, 'SASQ'];
  if (id) {
    queryNamespace.push(id);
  }
  queryNamespace.push('view');
  queryNamespace.push(match?.params?.id);

  const ky = useOkapiKy();
  const { data = {}, ...rest } = useQuery(
    queryNamespace,
    () => ky(`${endpoint}/${match?.params?.id}`).json(),
    {
      enabled: !!match?.params?.id
    }
  );

  useImperativeHandle(ref, () => (
    {
      queryProps: {
        data,
        ...rest
      }
    }
  ));

  return (
    <ViewComponent
      onClose={() => history.push(`${path}${location.search}`)}
      queryProps={{ ...rest }}
      resource={data}
      {...props}
    />
  );
});

SASQViewComponent.propTypes = {
  fetchParameters: PropTypes.object,
  history: PropTypes.object,
  id: PropTypes.string,
  location: PropTypes.object,
  match: PropTypes.object,
  path: PropTypes.string.isRequired,
  ViewComponent: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ]),
};

export default SASQViewComponent;
