# SASQRoute
A component designed to speed up the basic 3-pane layout setup process, SASQRoute is an all in one Routing, SASQ and MCL setup

## Basic Usage
```
import React from 'react';
import { FormattedMessage } from 'react-intl';

import { SASQRoute } from '@k-int/stripes-kint-components';
import ActionItem from '../components/ActionItem';

const ActionedRoute = ({ path }) => {

  const fetchParameters = {
    endpoint: "remote-sync/feedback/done",
    itemEndpoint: "remote-sync/feedback",
    SASQ_MAP: {
      searchKey: 'description',
      filterKeys: {
      }
    }
  };

  const resultColumns = [
  { propertyPath: "selected", label: " " },
  { propertyPath:"description", label: <FormattedMessage id="ui-remote-sync.prop.feedback.description" /> },
  { propertyPath:"status", label: <FormattedMessage id="ui-remote-sync.prop.feedback.status" /> },
  { propertyPath:"correlationId", label: <FormattedMessage id="ui-remote-sync.prop.feedback.correlationId" /> },
  { propertyPath:"caseIndicator", label: <FormattedMessage id="ui-remote-sync.prop.feedback.caseIndicator" /> }
];

  return (
    <SASQRoute
      fetchParameters={fetchParameters}
      id="actioned"
      resultColumns={resultColumns}
      path={path}
      ViewComponent={ActionItem}
    />
  );
};
```

NOTE - The following prop list is incomplete
## Props

Name | Type | Description | default | required
--- | --- | --- | --- | ---
fetchParameters | object | An object containing the parameters needed to make the fetches necessary for both the table and the view components. `endpoint` contains the main fetch endpoint, which regularly will be used for both fetching all data, and also for fetching an individual item (It is assumed that this endpoint will have `/{id}` appended to it.) If `itemEndpoint` is provided that will be used instead (this will also have `/{:id}` appended to it). `SASQ_MAP` is an object of the shape taken by `generateKiwtQuery`  | | ✓ |
id | string | A unique identifier for this route. IMPORTANT that this is unique, as it drives paneset logic  | | ✓ |
path | string | The main path for this route. In the above example the path is `remote-sync/actioned`. This component will set up the main 3 pane layout under this path, and also the view pane route under `remote-sync/actioned/:id` | | ✓ |
resultColumns | array | An array containing objects with a `propertyPath` and `label`. These will be used to drive the MCL columns.  | | ✓ |
ViewComponent | Element | The component to render on clicking a row entry.  | | ✓ |


