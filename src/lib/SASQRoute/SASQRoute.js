import { forwardRef, useImperativeHandle, useRef } from 'react';
import PropTypes from 'prop-types';

import {
  Route,
  Switch
} from 'react-router-dom';

import { SASQLookupComponent } from '../SASQLookupComponent';
import SASQViewComponent from '../SASQViewComponent';

const SASQRoute = forwardRef(({
  children,
  fetchParameters,
  path,
  ...props
}, ref) => {
  // Grab the SASQ_MAP and tweak it
  const { SASQ_MAP = {} } = fetchParameters;

  const lookupRef = useRef();
  const viewRef = useRef();

  useImperativeHandle(ref, () => ({
    ...lookupRef.current,
    ...viewRef.current
  }));

  if (!SASQ_MAP.perPage) {
    SASQ_MAP.perPage = 25;
  }

  SASQ_MAP.stats = true;

  // Reinsert the SASQ_MAP
  fetchParameters.SASQ_MAP = SASQ_MAP;

  return (
    <Route
      path={`${path}/:id?`}
      render={routeProps => {
        return (
          <SASQLookupComponent
            ref={lookupRef}
            {...routeProps}
            fetchParameters={fetchParameters}
            path={path}
            {...props}
          >
            <Switch>
              {children}
              <Route
                path={`${path}/:id`}
                render={innerProps => (
                  <SASQViewComponent
                    ref={viewRef}
                    {...innerProps}
                    fetchParameters={fetchParameters}
                    path={path}
                    ViewComponent={props.ViewComponent}
                    {...props}
                  />
                )}
              />
            </Switch>
          </SASQLookupComponent>
        );
      }}
    />
  );
});

SASQRoute.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.element,
    PropTypes.func
  ]),
  fetchParameters: PropTypes.object,
  path: PropTypes.string,
  ViewComponent: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node
  ])
};

export default SASQRoute;
