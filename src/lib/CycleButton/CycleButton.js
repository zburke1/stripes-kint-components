import { useState } from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';

import { IconButton } from '@folio/stripes/components';
import css from '../../../styles/CycleButton.css';

const CycleButton = ({
  buttons,
  startIndex = 0
}) => {
  const [index, setIndex] = useState(startIndex);

  const { className, onClick, ...buttonProps } = buttons[index];

  return (
    <IconButton
      {...buttonProps}
      className={classnames(
        className,
        css.buttonStyle,
      )}
      onClick={(e) => {
        if (onClick) {
          onClick(e);
        }

        setIndex(index !== (buttons?.length - 1) ? index + 1 : 0);
      }}
    />
  );
};

CycleButton.propTypes = {
  buttons: PropTypes.arrayOf(PropTypes.shape({
    className: PropTypes.object,
    icon: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  })),
  startIndex: PropTypes.number
};

export default CycleButton;
