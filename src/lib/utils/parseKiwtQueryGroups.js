const parseKiwtQueryGroups = (query) => {
  // Split out all operators and brackets, but keeping them all intact in the correct potition.
  const splitString = query.split(/(?<=\|\||&&|\(|\))|(?=\|\||&&|\(|\))/);
  // Keep track of what to skip over when wereturn from each level of nesting
  let skipCount = [0];


  // Ensure brackets are properly closed
  let unclosedBrackets = 0;
  const groupParser = (parseArray, nestLevel = 0) => {
    const group = [];
    // Iterate over each element in the array, recursively calling this when hitting brackets
    parseArray?.every((element, index) => {
      if (skipCount[nestLevel] > 0) {
        skipCount[nestLevel] -= 1;
        return true; // Equivalent to BREAK -- move onto next element
      }

      // Deal with unexpected extra closure
      if (nestLevel === 0 && element === ')') {
        throw new Error('Unexpected character \')\' found, stopping parse.');
      }

      // At the levels below all of the following will need
      skipCount = skipCount?.map((n, i) => {
        if (i < nestLevel) {
          return n + 1;
        }
        return n;
      });

      if (element !== '(' && element !== ')') {
        group?.push(element);
      } else if (element === ')') {
        // Remove the corresponding skipCount level because we're going down a nesting level
        skipCount.pop();
        unclosedBrackets -= 1;

        return false; // Equivalent to BREAK -- go up a nesting level
      } else if (element === '(') {
        // Add next index to skipcount for the nesting
        skipCount.push(0);
        unclosedBrackets += 1;

        group.push(groupParser(parseArray.slice(index + 1, parseArray?.length), nestLevel + 1));
      }

      // If all is well, continue on parsing
      return true;
    });

    return group;
  };

  const outputGroups = groupParser(splitString);

  // Deal with unclosed parentheses
  if (unclosedBrackets > 0) {
    throw new Error('Found unclosed paretheses, stopping parse.');
  }

  return outputGroups;
};

export default parseKiwtQueryGroups;
