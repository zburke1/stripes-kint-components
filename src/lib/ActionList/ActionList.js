import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';

import { Form } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';
import arrayMutators from 'final-form-arrays';

import ActionListFieldArray from './ActionListFieldArray';

const propTypes = {
  actionAssigner: PropTypes.func,
  columnMapping: PropTypes.object,
  contentData: PropTypes.arrayOf(PropTypes.object),
  creatableFields: PropTypes.object,
  createCallback: PropTypes.func,
  defaultNewObject: PropTypes.object,
  editableFields: PropTypes.object,
  fieldComponents: PropTypes.object,
  hideActionsColumn: PropTypes.bool,
  hideCreateButton: PropTypes.bool,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  labelOverrides: PropTypes.object,
  validateFields: PropTypes.object,
  visibleFields: PropTypes.arrayOf(PropTypes.string)
};

const ActionList = forwardRef(({
  actionAssigner,
  columnMapping,
  contentData,
  creatableFields = {},
  createCallback,
  defaultNewObject = {},
  editableFields = {},
  fieldComponents = {},
  hideActionsColumn = false,
  hideCreateButton,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  label,
  labelOverrides = {},
  validateFields,
  visibleFields,
  ...mclProps // Assume anything left over is to directly apply to the MCL
}, ref) => {
  const ActionListFieldArrayWithRef = (alfaProps) => (
    <ActionListFieldArray ref={ref} {...alfaProps} />
  );

  return (
    <>
      <Form
        enableReinitialize
        initialValues={{ contentData }}
        mutators={arrayMutators}
        onSubmit={() => null}
        subscription={{ contentData: true }}
      >
        {({ handleSubmit }) => (
          <form onSubmit={e => { e.preventDefault(); }}>
            <FieldArray
              actionAssigner={actionAssigner}
              columnMapping={columnMapping}
              component={ActionListFieldArrayWithRef}
              creatableFields={creatableFields}
              createCallback={createCallback}
              defaultNewObject={defaultNewObject}
              editableFields={editableFields}
              fieldComponents={fieldComponents}
              hideActionsColumn={hideActionsColumn}
              hideCreateButton={hideCreateButton}
              intlKey={passedIntlKey}
              intlNS={passedIntlNS}
              label={label}
              labelOverrides={labelOverrides}
              name="contentData"
              triggerFormSubmit={handleSubmit}
              validateFields={validateFields}
              visibleFields={visibleFields}
              {...mclProps}
            />
          </form>
        )}
      </Form>
    </>
  );
});

ActionList.propTypes = propTypes;

export default ActionList;
