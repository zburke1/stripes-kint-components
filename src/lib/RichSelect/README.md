# RichSelect
A component designed to render a dropdown, for a Select-style pick between rich object choices.

## Basic Usage
```
import { RichSelect } from '@k-int/stripes-kint-components'

  <RichSelect
    label="RichSelect"
    options={[
      {
        value: "option1",
        label: "Option 1",
        someOtherProp: true
      },
      {
        value: "option2",
        label: "Option 2",
        something: false
      }
    ]}
  />
```

## Usage with Ref
A hook is supplied, `useSelectedOption`, which will return `[ref, selectedOption]`. The object `selectedOption` will contain the full option object that is currently selected by RichSelect, thus avoiding the need to parse out form values and find the value within the options array provided manually for each RichSelect.


```
import { RichSelect, useSelectedOption } from '@k-int/stripes-kint-components'
  const [ref, selectedOption] = useSelectedOption();
  console.log("Selected option: %o", selectedOption);

  return (
    <RichSelect
      ref={ref}
      ...
    />
  );
```

## Props
Name | Type | Description | default | required
--- | --- | --- | --- | ---
ariaLabel | String | A string aria-label. This is only supposed to be used when a label will not be present, so that screen readers will still have a label to understand what the button is for. | | ✕ |
disabled | Boolean | Disables the default renderTrigger | false | ✕ |
id | String | A string id used for accessibility reasons | | ✓ |
label | String/Node | A visible label for the element. If not present, an ariaLabel should be supplied. | | ✕ |
onChange | Function | An onChange handler, which will be passed an event when an option is clicked. If this is being used in a form as a controlled component, the onChange will need to handle setting the form value, and currently the value will be set to the option selected momentarily before then being set to the value via the passed onChange. | | ✕ |
options | Array\<Object> | An array of option objects, corresponding to the choices available in the dropdown menu. These objects _must_ contain a `value` prop, and a `label` prop is highly recommended. Beyond this the option object can take any shape required. | [] | ✕ |
placeholder | Node/String | A custom placeholder in the default trigger button when no value is selected | "Please select an option" | ✕ |
renderMenu | Function | A function to replace the entire renderMenu for the dropdown. Provided props are all those provided by Stripes `Dropdown` component normally, as well as `selectedOption`, the object containing the currently selectedOption, and `changeField`, a function which takes a string value and natively sets the inner `input`, triggering the onChange and such. | | ✕ |
renderOption | Function | A function which instead of replacing the entire renderMenu, simply replaces the rendered option button. The function takes a single prop containing the option to be rendered, and expects a node to be returned. | `(opt) => opt?.label ?? opt?.value` | ✕ |
renderTrigger | Function | A function to replace the default render trigger provided by RichSelect. Takes all properties Stripes `Dropdown` provides to the renderTrigger prop, as well as `selectedOption`, the current selected option object. | | ✕ |
required | Boolean | A boolean used to set required styling on the label | false | ✕ |
value | String | A value property to allow this component to be used as a controller form component | | ✕ |