import React, { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';

import { ConfirmationModal } from '@folio/stripes/components';
import { CalloutContext } from '@folio/stripes/core';

import { useKintIntl, useMutateRefdataCategory, useRefdata } from '../hooks';

import ActionList from '../ActionList';
import { required } from '../validators';
import { parseErrorResponse } from '../utils';

const propTypes = {
  afterQueryCalls: PropTypes.object,
  catchQueryCalls: PropTypes.object,
  displayConditions: PropTypes.shape({
    create: PropTypes.bool,
    delete: PropTypes.bool,
    view: PropTypes.bool,
  }),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  labelOverrides: PropTypes.object,
  refdataEndpoint: PropTypes.string
};

const EditableRefdataCategoryList = ({
  afterQueryCalls,
  catchQueryCalls,
  /*
   * Set of extra booleans for controlling access to actions
   * create/delete (View should be handled externally)
   * This will not overwrite "internal" behaviour, ie setting
   * delete to 'true' here would still not render a delete button
   * for a refdata category that has refdata values.
   */
  displayConditions = {
    create: true,
    delete: true,
  },
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  label,
  labelOverrides = {}, // An object containing translation alternatives
  refdataEndpoint
}) => {
  /* A component that allows for editing of refdata categories */
  const callout = useContext(CalloutContext);
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const {
    create: createCondition = true,
    delete: deleteCondition = true,
  } = displayConditions;

  // fetch refdata values
  const { data: refdata = [], isLoading: isRefdataLoading } = useRefdata({
    endpoint: refdataEndpoint,
    returnQueryObject: true
  });

  const [contentData, setContentData] = useState([]);
  const [deleteModal, setDeleteModal] = useState({
    visible: false,
    refdata: null,
  });

  const sortByDesc = (a, b) => (a.desc.localeCompare(b.desc));

  useEffect(() => {
    if (!isRefdataLoading) {
      setContentData(refdata?.sort(sortByDesc) ?? []);
    }
  }, [isRefdataLoading, refdata]);

  // Edit and Create will use the same POST mutation
  const { delete: deleteRefdataCategory, post: createRefdataCategory } = useMutateRefdataCategory({
    afterQueryCalls: {
      delete: json => {
        if (afterQueryCalls?.delete) {
          afterQueryCalls.delete(json);
        }
      },
      post: json => {
        if (afterQueryCalls?.post) {
          afterQueryCalls.post(json);
        }
      }
    },
    catchQueryCalls: {
      // Default delete behaviour is to fire a callout, either with kint-components default message
      // or one provided in labelOverrides, which is passed the error message and refdata in question
      delete: async (err) => {
        const errorResp = await parseErrorResponse(err.response);
        callout.sendCallout({
          message: kintIntl.formatKintMessage({
            id: 'refdataCategory.deleteRefdataCategory.errorMessage',
            overrideValue: labelOverrides?.deleteError
          },
          {
            label: deleteModal?.refdata?.label,
            error: errorResp?.message
          }),
          type: 'error',
        });
      },
      ...catchQueryCalls // override defaults here
    },
    endpoint: refdataEndpoint,
    id: refdata?.id,
    queryParams: {
      delete: {
        enabled: !!refdata
      },
      post: {
        enabled: !!refdata
      }
    }
  });

  if (isRefdataLoading) {
    return 'loading';
  }

  // This is the function which will take a row in the table and assign the relevant actions to it
  const actionAssigner = (rowData) => {
    const actionArray = [];

    if (!rowData?.values?.length && deleteCondition) {
      actionArray.push({
        name: 'delete',
        label: kintIntl.formatKintMessage({
          id: 'delete',
          overrideValue: labelOverrides?.delete
        }),
        icon: 'trash',
        callback: (data) => setDeleteModal({ visible: true, refdata: data }),
        ariaLabel: (data) => kintIntl.formatKintMessage(
          {
            id: 'refdataCategory.deleteAriaLabel',
            overrideValue: labelOverrides?.deleteAriaLabel
          },
          { label: data?.label }
        ),
      });
    }
    return actionArray;
  };

  return (
    <>
      <ActionList
        actionAssigner={actionAssigner}
        columnMapping={{
          desc: kintIntl.formatKintMessage({
            id: 'refdataCategory.refdataCategory',
            overrideValue: labelOverrides?.refdataCategory
          }),
          values: kintIntl.formatKintMessage({
            id: 'refdataCategory.noOfValues',
            overrideValue: labelOverrides?.noOfValues
          }),
        }}
        contentData={contentData}
        creatableFields={{
          values: () => false
        }}
        createCallback={!createCondition ?
          null :
          (data) => createRefdataCategory(data)
        }
        formatter={{
          values: (rowData) => rowData?.values?.length
        }}
        /* Hide actions column when no permissions, or no deletable refdata categories */
        hideActionsColumn={
          (!createCondition && !deleteCondition) ||
          !contentData?.find(cd => cd?.values?.length === 0)
        }
        label={label}
        validateFields={{
          desc: () => required
        }}
        visibleFields={['desc', 'values']}
      />
      <ConfirmationModal
        confirmLabel={
          kintIntl.formatKintMessage({
            id: 'delete',
            overrideValue: labelOverrides?.delete
          })
        }
        heading={
          kintIntl.formatKintMessage({
            id: 'refdataCategory.deleteRefdataCategory',
            overrideValue: labelOverrides?.deleteRefdataCategory
          })
        }
        message={
          kintIntl.formatKintMessage({
            id: 'refdataCategory.deleteRefdataCategory.confirmMessage',
            overrideValue: labelOverrides?.deleteRefdataCategoryMessage
          }, { name: deleteModal?.refdata?.desc })
        }
        onCancel={() => setDeleteModal({ visible: false, refdata: null })}
        onConfirm={() => {
          deleteRefdataCategory(deleteModal?.refdata?.id);
          setDeleteModal({ visible: false, refdata: null });
        }}
        open={deleteModal?.visible}
      />
    </>
  );
};

EditableRefdataCategoryList.propTypes = propTypes;

export default EditableRefdataCategoryList;
