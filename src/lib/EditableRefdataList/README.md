# EditableRefdataList
A component designed to render an editable list of refdata, which is editable if the refdata has property `internal: false`.
This will create an `ActionList` complete with `edit`, `delete` and `create` actions, and allow the user to edit the `value` and `label` of refdataValues.

## Basic Usage
```
import { EditableRefdataList } from '@k-int/stripes-kint-components';

...
<EditableRefdataList
  desc="PublicationRequest.RequestStatus"
  refdataEndpoint="oa/refdata"
/>
```

## Props

Name | Type | Description | default | required
--- | --- | --- | --- | ---
afterQueryCalls | object | An object of the form `{delete: func1, put: func2}` where `func1`/`func2` are functions to be called after the `delete`/`put`. | | ✕ |
allowSpecial | boolean | A boolean to turn off/on the stripping out of special characters and replacing them with `_`. Generally turning this on will cause problems with queries to KIWT endpoints and internally with Stripes filters, where a refdata value includes `&&`, `||`, `\` or `"`. | false | ✕ |
catchQueryCalls | object | An object of the form `{delete: func1, put: func2}` where `func1`/`func2` are functions to be called with a HTTPError object when the `delete`/`put` calls fail. | `{delete: <Fires a callout of the format "<strong>Error: {label}</strong> was not deleted. {error}">}` | | ✕ |
desc | String | A string corresponding to the refdataValue category, usually of the form `DomainClass.Field`. See `useRefdata` for more information. | | ✕ |
displayConditions | object | An object of the form `{delete: false, edit: true, create: true}`, with keys `delete`, `edit` and `create`, and a boolean value. These will each default to true if not specified. These booleans will act as an additional check before rendering the actions for create/delete/edit, as well as whether or not to render the actions column at all. | `{delete: true, edit: true, create: true}` | ✕ |
endpoint | String | A string containing the path to the endpoint for refdata | | ✓ |
labelOverrides | object | An object containing translation overrides. Currently accepts: `deleteError: (err, object) => { ... output a callout message }`. | | ✕ |
