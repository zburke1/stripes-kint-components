import PropTypes from 'prop-types';

import { Form } from 'react-final-form';
import { Button, Modal, ModalFooter } from '@folio/stripes/components';
import { useKintIntl } from '../hooks';

const FormModal = ({
  children,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  modalProps: { footer, onClose, ...modalProps },
  onSubmit,
  ...formProps
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  return (
    <Form
      onSubmit={onSubmit}
      {...formProps}
    >
      {({ handleSubmit, form: { getState, restart } }) => {
        const formState = getState();
        const handleClose = (e) => {
          onClose(e);
          restart();
        };

        const renderFooter = () => {
          if (footer) {
            return footer({ formState, handleSubmit, handleClose });
          }

          const { invalid, pristine, submitting } = formState;
          return (
            <ModalFooter>
              <Button
                buttonStyle="primary"
                disabled={submitting || invalid || pristine}
                marginBottom0
                onClick={handleSubmit}
                type="submit"
              >
                {kintIntl.formatKintMessage({
                  id: 'saveAndClose',
                  overrideValue: labelOverrides.saveAndClose
                })}
              </Button>
              <Button
                marginBottom0
                onClick={handleClose}
              >
                {kintIntl.formatKintMessage({
                  id: 'cancel',
                  overrideValue: labelOverrides.cancel
                })}
              </Button>
            </ModalFooter>
          );
        };

        return (
          <form
            onSubmit={handleSubmit}
          >
            <Modal
              enforceFocus={false}
              footer={renderFooter()}
              onClose={handleClose}
              {...modalProps}
            >
              {children}
            </Modal>
          </form>
        );
      }}
    </Form>
  );
};

FormModal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.func,
  ]),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  modalProps: PropTypes.shape({
    footer: PropTypes.func,
    onClose: PropTypes.func,
  }),
  onSubmit: PropTypes.func
};

export default FormModal;
