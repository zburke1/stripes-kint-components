import React from 'react';
import PropTypes from 'prop-types';
import { Field } from 'react-final-form';

import { SettingField } from './SettingField';

const EditableSettingsListFieldArray = ({
  allowEdit = true,
  data,
  fields,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  onSave
}) => {
  const handleSave = (index) => {
    const setting = fields.value[index];
    return onSave(setting);
  };

  return (
    fields.map((setting, i) => {
      return (
        <Field
          key={setting}
          allowEdit={allowEdit}
          component={SettingField}
          data-testid={`editableSettingsListFieldArray[${i}]`}
          intlKey={passedIntlKey}
          intlNS={passedIntlNS}
          labelOverrides={labelOverrides}
          name={setting}
          onSave={() => handleSave(i)}
          settingData={{
            currentSetting: fields.value[i],
            ...data
          }}
        />
      );
    })
  );
};

EditableSettingsListFieldArray.propTypes = {
  allowEdit: PropTypes.bool,
  fields: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  onSave: PropTypes.func,
  data: PropTypes.shape({
    refdatavalues: PropTypes.arrayOf(PropTypes.object)
  }),
  mutators: PropTypes.object,
  initialValues: PropTypes.object
};

export default EditableSettingsListFieldArray;
