import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Card,
  InfoPopover,
} from '@folio/stripes/components';

import EditSettingValue from './EditSettingValue';
import RenderSettingValue from './RenderSettingValue';
import { SettingsContext } from '../../contexts';
import { useKintIntl, useRefdata, useTemplates } from '../../hooks';
import { toCamelCase } from '../../utils';

import renderHelpTextCSS from '../../../../styles/renderHelpText.css';

const SettingField = (settingFieldProps) => {
  const {
    allowEdit = true,
    intlKey: passedIntlKey,
    intlNS: passedIntlNS,
    labelOverrides = {},
    onSave,
    settingData: {
      currentSetting
    } = {}
  } = settingFieldProps;

  const [editing, setEditing] = useState(false);
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);
  const { refdataEndpoint, templateEndpoint } = useContext(SettingsContext);

  const {
    0: {
      values: refdata = []
    } = {}
  } = useRefdata({
    endpoint: refdataEndpoint,
    desc: currentSetting.vocab,
    queryParams: {
      enabled: !!currentSetting?.vocab && currentSetting.settingType === 'Refdata'
    }
  });

  const templates = useTemplates({
    context: currentSetting.vocab,
    endpoint: templateEndpoint,
    queryParams: {
      enabled: !!currentSetting?.vocab && currentSetting.settingType === 'Template'
    }
  });

  const renderHelpText = (helpTextId) => {
    return (
      <div className={renderHelpTextCSS.help}>
        {
          kintIntl.formatKintMessage({
            id: helpTextId
          }, {
            b: (chunks) => <b>{chunks}</b>,
            i: (chunks) => <i>{chunks}</i>,
            em: (chunks) => <em>{chunks}</em>,
            strong: (chunks) => <strong>{chunks}</strong>,
            span: (chunks) => <span>{chunks}</span>,
            div: (chunks) => <div>{chunks}</div>,
            p: (chunks) => <p>{chunks}</p>,
            ul: (chunks) => <ul>{chunks}</ul>,
            ol: (chunks) => <ol>{chunks}</ol>,
            li: (chunks) => <li>{chunks}</li>,
          })
        }
      </div>
    );
  };

  const renderEditButton = () => {
    const EditText = editing ?
      kintIntl.formatKintMessage({
        id: 'save'
      }) :
      kintIntl.formatKintMessage({
        id: 'edit'
      });
    if (allowEdit) {
      return (
        <Button
          marginBottom0
          onClick={(e) => {
            e.preventDefault();
            return (
              editing ?
                onSave().then(() => setEditing(false)) :
                setEditing(true)
            );
          }}
        >
          {EditText}
        </Button>
      );
    }
    return null;
  };

  let RenderFunction;
  if (editing === false) {
    RenderFunction = RenderSettingValue;
  } else {
    RenderFunction = EditSettingValue;
  }

  const id = `settings.${toCamelCase(currentSetting.section)}.${toCamelCase(currentSetting.key)}.help`;


  const renderHeaderStart = () => {
    const settingName = Object.keys(currentSetting).length > 0 ?
    kintIntl.formatKintMessage({
      id: `settings.${toCamelCase(currentSetting.section)}.${toCamelCase(currentSetting.key)}`
    }) :
    kintIntl.formatKintMessage({
      id: 'loading'
    });

    return (
      <>
        {
          kintIntl.messageExists(id) && <InfoPopover content={renderHelpText(id)} />}
        {settingName}
      </>
    );
  };

  return (
    <Card
      headerEnd={renderEditButton()}
      headerStart={renderHeaderStart()}
      roundedBorder
    >
      <RenderFunction
        currentSetting={currentSetting}
        intlKey={passedIntlKey}
        intlNS={passedIntlNS}
        labelOverrides={labelOverrides}
        refdata={refdata}
        templates={templates}
        {...settingFieldProps}
      />
    </Card>
  );
};

SettingField.propTypes = {
  allowEdit: PropTypes.bool,
  settingData: PropTypes.shape({
    refdatavalues: PropTypes.arrayOf(PropTypes.object),
    currentSetting: PropTypes.object,
    templates: PropTypes.arrayOf(PropTypes.object),
  }),
  input: PropTypes.object,
  onSave: PropTypes.func
};

export default SettingField;
