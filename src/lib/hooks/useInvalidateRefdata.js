import { useQueryClient } from 'react-query';

import { refdataQueryKey } from '../utils';

const useInvalidateRefdata = (desc) => {
  const queryClient = useQueryClient();

  return () => queryClient.invalidateQueries(refdataQueryKey(desc));
};

export default useInvalidateRefdata;
