import PropTypes from 'prop-types';

import {
  Card,
  IconButton,
  InfoPopover,
  Tooltip
} from '@folio/stripes/components';

import CustomPropertyField from './CustomPropertyField';
import { useKintIntl } from '../../hooks';

const CustomPropertyFormCard = ({
  availableCustomProperties,
  customProperty,
  customPropertyType,
  customProperties,
  handleDeleteCustomProperty,
  index,
  internalPropertyCounter,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  name,
  onChange,
  setCustomProperties,
  value
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const getHeader = () => {
    if (customPropertyType === 'optional') {
      return kintIntl.formatKintMessage({
        id: 'customProperty.optionalPropertyHeader',
        overrideValue: labelOverrides.optionalPropertyHeader,
      }, { number: internalPropertyCounter, name: customProperty.value });
    }

    return kintIntl.formatKintMessage({
      id: 'customProperty.primaryPropertyHeader',
      overrideValue: labelOverrides.primaryPropertyHeader,
      fallbackMessage: customProperty.label
    }, { number: internalPropertyCounter, name: customProperty.value });
  };

  const getTooltipText = () => (
    kintIntl.formatKintMessage({
      id: 'customProperty.remove',
      overrideValue: labelOverrides.remove
    }, { number: internalPropertyCounter, name: customProperty.value })
  );

  return (
    <Card
      key={customProperty.value}
      data-testid="custom-property-form-card"
      headerEnd={customPropertyType === 'optional' ?
        <Tooltip
          id={`customProperty-${customProperty.value}-${index}`}
          text={getTooltipText()}
        >
          {({ ref, ariaIds }) => (
            <IconButton
              ref={ref}
              aria-labelledby={ariaIds.text}
              icon="trash"
              id={`edit-customproperty-${index}-delete`}
              onClick={() => handleDeleteCustomProperty(customProperty, index)}
            />
          )}
        </Tooltip> :
        null
      }
      headerStart={
        <strong>
          {getHeader()}
          {customProperty.description ? (
            <InfoPopover
              content={customProperty.description}
            />
          ) : null}
        </strong>
      }
    >
      <CustomPropertyField
        {...{
          availableCustomProperties,
          customProperty,
          customPropertyType,
          customProperties,
          index,
          internalPropertyCounter,
          intlKey: passedIntlKey,
          intlNS: passedIntlNS,
          labelOverrides,
          name,
          onChange,
          setCustomProperties,
          value
        }}
      />
    </Card>
  );
};

CustomPropertyFormCard.propTypes = {
  availableCustomProperties: PropTypes.arrayOf(PropTypes.object),
  customProperty: PropTypes.object,
  customPropertyType: PropTypes.string,
  customProperties: PropTypes.arrayOf(PropTypes.object),
  handleDeleteCustomProperty: PropTypes.func,
  index: PropTypes.number,
  internalPropertyCounter: PropTypes.number,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  name: PropTypes.string,
  onChange: PropTypes.func,
  setCustomProperties: PropTypes.func,
  value: PropTypes.object
};

export default CustomPropertyFormCard;
