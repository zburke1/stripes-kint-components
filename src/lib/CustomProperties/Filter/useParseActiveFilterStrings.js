import useOperators from './useOperators';

const useParseActiveFilterStrings = (filterStrings, passedIntlKey, passedIntlNS, labelOverrides = {}) => {
  const operators = useOperators(null, passedIntlKey, passedIntlNS, labelOverrides).map(o => o.value);

  let numberOfFilters = 0;
  const filters = filterStrings.map(filter => {
    let customProperty;
    const rules = filter.split('||').map(ruleString => {
      // ruleString is constructed in this.handleSubmit passed to CustomPropertyFiltersForm
      // and has shape "customProperties.foo.value!=42"
      // OR customProperties.foo.value.id==1234, so make sure to disregard the .id
      const [customPropertyPath, rule] = ruleString.split('.value');
      const idSafeRule = rule.replace('.id', '');

      customProperty = customPropertyPath.replace('customProperties.', '');

      const operator = operators.find(o => idSafeRule.startsWith(o)) ?? '';
      const value = idSafeRule.substring(operator.length);

      numberOfFilters += 1;

      return { operator, value };
    });

    return {
      customProperty,
      rules,
    };
  });

  return {
    filters,
    numberOfFilters
  };
};

export default useParseActiveFilterStrings;
