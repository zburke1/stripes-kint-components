import { Datepicker, Select, TextField } from '@folio/stripes/components';
import * as CUSTPROP_TYPES from '../../constants/customProperties';
import { invalidNumber, required as requiredValidator } from '../../validators';

const {
  REFDATA_CLASS_NAME: REFDATA,
  MULTI_REFDATA_CLASS_NAME: MULTIREFDATA,
  DECIMAL_CLASS_NAME: DECIMAL,
  INTEGER_CLASS_NAME: INTEGER,
  DATE_CLASS_NAME: DATE,
  TEXT_CLASS_NAME: TEXT,
} = CUSTPROP_TYPES;

const useValueProps = (
  custPropDefinition,
  intlKey,
  intlNS,
  labelOverrides = {},
) => {
  const props = {
    validate: requiredValidator
  };

  switch (custPropDefinition.type) {
    case INTEGER:
    case DECIMAL:
      props.component = TextField;
      props.type = 'number';
      props.validate = (value) => invalidNumber(value, intlKey, intlNS, labelOverrides);
      break;
    case REFDATA:
      props.component = Select;
      props.dataOptions = custPropDefinition.category.values.map(rdv => ({ label: rdv.label, value: rdv.id }));
      props.placeholder = ' ';
      break;
    case MULTIREFDATA:
      props.component = Select;
      props.dataOptions = custPropDefinition.category.values.map(mrdv => ({ label: mrdv.label, value: mrdv.id }));
      props.placeholder = ' ';
      break;
    case DATE:
      props.component = Datepicker;
      props.backendDateStandard = 'YYYY-MM-DD';
      props.timeZone = 'UTC';
      props.usePortal = true;
      break;
    case TEXT:
    default:
      props.component = TextField;
      break;
  }

  return props;
};

export default useValueProps;
