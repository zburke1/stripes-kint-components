import PropTypes from 'prop-types';

import { Field, useForm, useFormState } from 'react-final-form';
import { FieldArray } from 'react-final-form-arrays';

import {
  Button,
  Col,
  Label,
  Row,
  Select
} from '@folio/stripes/components';

import { groupCustomPropertiesByCtx } from '../../utils';
import { required as requiredValidator } from '../../validators';

import CustomPropertiesRule from './CustomPropertiesRule';
import { useKintIntl } from '../../hooks';

const CustomPropertiesFilterField = ({
  customProperties = [],
  fields,
  index,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  name,
}) => {
  const groupedCustomProperties = groupCustomPropertiesByCtx(customProperties);
  const { change, mutators: { push } } = useForm();
  const { values } = useFormState();
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  return (
    <>
      <Field
        id={`input-custprop-${index}`}
        label={
          kintIntl.formatKintMessage({
            id: 'customProperty',
            overrideValue: labelOverrides.customProperty
          })
        }
        name={`${name}.customProperty`}
        placeholder=" "
        render={fieldProps => {
          const sortedGroupedCustProps = Object.entries(groupedCustomProperties)?.sort((a, b) => {
            if (a[0] === 'isNull') return -1; // Make sure noContext is at top

            if (a[0].toLowerCase() < b[0].toLowerCase()) return -1;
            if (a[0].toLowerCase() > b[0].toLowerCase()) return 1;
            return 0;
          });

          return (
            <Select
              {...fieldProps}
              placeholder={null} // placeholder default causes issues
            >
              <option key="empty-option" value=""> </option>
              {
                sortedGroupedCustProps.map(([key, value]) => {
                  return (
                    <optgroup
                      key={`custom-property-optgroup-${key}`}
                      label={key === 'isNull' ? '-' : key}
                    >
                      {value.map(v => {
                        return (
                          <option
                            key={v.id}
                            value={v.name}
                          >
                            {v.retired ?
                              kintIntl.formatKintMessage({
                                id: 'customProperty.retiredName',
                                overrideValue: labelOverrides.retiredName
                              }, { name: v.label }) :
                              v.label
                            }
                          </option>
                        );
                      })}
                    </optgroup>
                  );
                })
              }
            </Select>
          );
        }}
        required
        validate={requiredValidator}
      />
      {/* This next div is rendered so that it can be referred to using aria-labelledby */}
      <div
        data-testid={`selected-custprop-name-${index}`}
        id={`selected-custprop-name-${index}`}
        style={{ display: 'none' }}
      >
        {customProperties.find(t => t.name === fields.value[index]?.customProperty)?.label ?? ''}
      </div>
      <Row>
        <Col xs={2} />
        <Col xs={4}>
          <Label id="rule-column-header-comparator" required>
            {kintIntl.formatKintMessage({
                id: 'comparator',
                overrideValue: labelOverrides.comparator
            })}
          </Label>
        </Col>
        <Col xs={4}>
          <Label id="rule-column-header-value" required>
            {kintIntl.formatKintMessage({
                id: 'value',
                overrideValue: labelOverrides.value
            })}
          </Label>
        </Col>
        <Col xs={2} />
      </Row>
      <FieldArray name={`${name}.rules`}>
        {({ fields: ruleFields }) => ruleFields.map((ruleFieldName, ruleFieldIndex) => (
          <CustomPropertiesRule
            key={ruleFieldName}
            ariaLabelledby={`selected-custprop-name-${index}`}
            clearRuleValue={() => change(`filters[${index}].rules[${ruleFieldIndex}].value`, '')}
            custPropDefinition={customProperties.find(t => t.name === fields.value[index].customProperty)}
            index={ruleFieldIndex}
            labelOverrides={labelOverrides}
            name={ruleFieldName}
            onDelete={() => ruleFields.remove(ruleFieldIndex)}
            value={values.filters[index]?.rules[ruleFieldIndex]}
          />
        ))}
      </FieldArray>
      <Button
        data-test-add-rule-btn
        disabled={!fields.value[index]?.customProperty}
        onClick={() => push(`${name}.rules`)}
      >
        {kintIntl.formatKintMessage({
          id: 'addRule',
          overrideValue: labelOverrides.addRule
        })}
      </Button>
    </>
  );
};

CustomPropertiesFilterField.propTypes = {
  customProperties: PropTypes.arrayOf(PropTypes.object),
  fields: PropTypes.object,
  index: PropTypes.number,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  name: PropTypes.string
};

export default CustomPropertiesFilterField;
