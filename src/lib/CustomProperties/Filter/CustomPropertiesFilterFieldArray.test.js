import React from 'react';

import { TestForm } from '@folio/stripes-erm-testing';
import { waitFor } from '@testing-library/react';
import { Button } from '@folio/stripes-testing';
import { FieldArray } from 'react-final-form-arrays';

import CustomPropertiesFilterFieldArray from './CustomPropertiesFilterFieldArray';
import customProperties from '../../../../test/jest/customProperties';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('../../hooks');
jest.mock('./CustomPropertiesFilterField', () => () => <div>CustomPropertiesFilterField</div>);
const onSubmit = jest.fn();

let renderComponent;
describe('CustomPropertiesFilterFieldArray', () => {
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <TestForm
        onSubmit={onSubmit}
      >
        <FieldArray
          component={CustomPropertiesFilterFieldArray}
          customProperties={customProperties}
          name="customProperties"
        />
      </TestForm>
    );
  });

  test('renders CustomPropertiesFilterField component', () => {
    const { getByText } = renderComponent;
    waitFor(() => expect(getByText('CustomPropertiesFilterField')).toBeInTheDocument());
  });

  test('renders the Add custom property filter button', async () => {
    await Button('Add custom property filter').exists();
  });

  test('renders the submit button', async () => {
    await Button('Submit').exists();
  });
});
