import { useState } from 'react';
import PropTypes from 'prop-types';

import {
  Accordion,
  FilterAccordionHeader,
  Layout,
  Spinner
} from '@folio/stripes/components';
import { useCustomProperties, useKintIntl } from '../../hooks';
import useParseActiveFilterStrings from './useParseActiveFilterStrings';
import CustomPropertiesFilterForm from './CustomPropertiesFilterForm';
import { MULTI_REFDATA_CLASS_NAME, REFDATA_CLASS_NAME } from '../../constants/customProperties';

const CustomPropertiesFilter = ({
  activeFilters: {
    customProperties: custPropFilters,
    ...restOfFilters
  },
  customPropertiesEndpoint,
  filterHandlers,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {}
 }) => {
  const [editingFilters, setEditingFilters] = useState(false);
  const openEditModal = () => setEditingFilters(true);
  const closeEditModal = () => setEditingFilters(false);

  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const { data: custprops, isLoading } = useCustomProperties({
    endpoint: customPropertiesEndpoint,
    returnQueryObject: true,
    options: {
      sort: [
        { path: 'ctx' }, // Group by ctx
        { path: 'retired' }, // Place retired custprops at the end
        { path: 'weight' }, // Within those groups, sort by weight first
        { path: 'label' } // Within those groups, sort by label
      ],
    }
  });
  const parsedFilterData = useParseActiveFilterStrings(custPropFilters || [], passedIntlKey, passedIntlNS, labelOverrides);

  if (isLoading) {
    return (
      <Accordion
        closedByDefault
        header={FilterAccordionHeader}
        id="clickable-custprop-filter"
        label={
          kintIntl.formatKintMessage({
            id: 'customProperties',
            overrideValue: labelOverrides.customProperties,
          })
        }
        separator={false}
      >
        <Spinner />
      </Accordion>
    );
  }

  const handleSubmit = values => {
    const { filters = [] } = values;

    const filterStrings = filters
      .filter(filter => filter.rules)
      .map(filter => {
        const relevantCustomProperty = custprops?.filter(cp => cp.name === filter.customProperty)?.[0] ?? {};

        const filterByValueId = (
          relevantCustomProperty?.type === MULTI_REFDATA_CLASS_NAME ||
          relevantCustomProperty?.type === REFDATA_CLASS_NAME
        );

        return (
          filter.rules
            .map(rule => `customProperties.${filter.customProperty}.value${filterByValueId ? '.id' : ''}${rule.operator}${rule.value ?? ''}`)
            .join('||')
        );
      });

    filterHandlers.state({ ...restOfFilters, customProperties: filterStrings });
    setEditingFilters(false);

    return Promise.resolve();
  };

  return (
    <Accordion
      closedByDefault
      displayClearButton={(parsedFilterData?.numberOfFilters ?? 0) > 0}
      header={FilterAccordionHeader}
      id="clickable-custprop-filter"
      label={
        kintIntl.formatKintMessage({
          id: 'customProperties',
          overrideValue: labelOverrides.customProperties,
        })
      }
      onClearFilter={() => filterHandlers.state({ ...restOfFilters, customProperties: [] })}
      separator={false}
    >
      <Layout className="padding-bottom-gutter">
        {
          kintIntl.formatKintMessage({
            id: 'customProperty.filtersApplied',
            overrideValue: labelOverrides.filtersApplied,
          }, { count: (parsedFilterData?.numberOfFilters ?? 0) })
        }
      </Layout>
      <CustomPropertiesFilterForm
        customProperties={custprops}
        editingFilters={editingFilters}
        filters={parsedFilterData?.filters}
        handlers={{
          closeEditModal,
          openEditModal
        }}
        labelOverrides={labelOverrides}
        onSubmit={handleSubmit}
      />
    </Accordion>
  );
};

CustomPropertiesFilter.propTypes = {
  activeFilters: PropTypes.object,
  customPropertiesEndpoint: PropTypes.string,
  filterHandlers: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object
};

export default CustomPropertiesFilter;
