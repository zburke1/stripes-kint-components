import PropTypes from 'prop-types';
import CustomPropertiesViewCtx from './CustomPropertiesViewCtx';

const CustomPropertiesView = ({
  contexts = [],
  customProperties,
  customPropertiesEndpoint,
  id,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
}) => {
  return (
    contexts.map(ctx => (
      <CustomPropertiesViewCtx
        key={`customPropertiesView-${ctx}`}
        {...{
          ctx,
          customProperties,
          customPropertiesEndpoint,
          id,
          intlKey: passedIntlKey,
          intlNS: passedIntlNS,
          labelOverrides
        }}
      />
    ))
  );
};

CustomPropertiesView.propTypes = {
  contexts: PropTypes.arrayOf(PropTypes.string),
  customProperties: PropTypes.object,
  customPropertiesEndpoint: PropTypes.string,
  id: PropTypes.string,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  nameOverride: PropTypes.string
};

export default CustomPropertiesView;
