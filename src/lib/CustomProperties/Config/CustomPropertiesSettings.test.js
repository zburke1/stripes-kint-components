import React from 'react';

import { Button, Pane, PaneHeader } from '@folio/stripes-testing';
import { MemoryRouter } from 'react-router-dom';

import CustomPropertiesSettings from './CustomPropertiesSettings';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('../../hooks');

jest.mock('../../FormModal/FormModal', () => () => <div>FormModal</div>);
jest.mock('./CustomPropertiesLookup', () => () => <div>CustomPropertiesLookup</div>);

describe('CustomPropertiesSettings', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <MemoryRouter>
        <CustomPropertiesSettings
          contextFilterOptions={[
            {
              'value': '',
              'label': 'All'
            },
            {
              'value': 'isNull',
              'label': 'None'
            }
          ]}
          customPropertiesEndpoint="erm/custprops"
          refdataEndpoint="erm/refdata"
        />
      </MemoryRouter>
    );
  });

  it('renders FormModal component ', () => {
    const { getByText } = renderComponent;
    expect(getByText('FormModal')).toBeInTheDocument();
  });

  it('renders CustomPropertiesLookup component ', () => {
    const { getByText } = renderComponent;
    expect(getByText('CustomPropertiesLookup')).toBeInTheDocument();
  });

  it('renders the expected New button', async () => {
    await Button('New').exists();
  });

  it('renders the expected Pane ', async () => {
    await Pane('Custom properties').is({ visible: true });
  });

  it('renders the expected Pane header', async () => {
    await PaneHeader('Custom properties').is({ visible: true });
  });
});
